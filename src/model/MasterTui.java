package model;

public class MasterTui {
	
	private String navn;
	private int age; 
	private int strength; 
	
	public MasterTui(String navn, int age, int strength) {
		this.navn = navn;
		this.age = age;
		this.strength = strength;
	}
	
	public String getNavn() {
		return this.navn;
	}
	
	
}
