package model;

public class Universet {
	
	private int km;
	private String info;

	public static void main(String[] args) {
		Universet Jorden = new Universet(40000, "Jorden");
		Universet Mars = new Universet(40000, "Mars");
		Universet Måne = new Universet(40000, "Måne");
		
	}
	
	public Universet(int km, String info) {
		this.km = km;
		this.info = info;
	}

	public int getKm() {
		return km;
	}

	public void setKm(int km) {
		this.km = km;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	

}
