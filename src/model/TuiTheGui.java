package model;

public class TuiTheGui {

	private int skala;
	private String type;

	public TuiTheGui(int skala, String type) {
		super();
		this.skala = skala;
		this.type = type;
	}

	public int getSkala() {
		return skala;
	}

	public void setSkala(int skala) {
		this.skala = skala;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int howMuchTui(int gange) {
		skala = skala * gange;
		return skala;
	}

	public void oneMoreTui(int skala) {
		skala++;
	}

}
